#!/bin/zsh

# A two level prompt with git intergration
#  ⌠ zac@LinDevPower ~/usr/lib/zshrc zshrc
#  ⌡ |0|                                               master [ 11/17/19  5:57PM ]

autoload add-zsh-hook
add-zsh-hook precmd Z_Prompt
autoload -U colors && colors

# set colors for use in prompts (modern zshs allow for the use of %F{red}foo%f
# in prompts to get a red "foo" embedded, but it's good to keep these for
# backwards compatibility).
if autoload colors && colors 2>/dev/null ; then
    BLUE="%{${fg[blue]}%}"
    RED="%{${fg_bold[red]}%}"
    GREEN="%{${fg[green]}%}"
    CYAN="%{${fg[cyan]}%}"
    MAGENTA="%{${fg[magenta]}%}"
    YELLOW="%{${fg[yellow]}%}"
    WHITE="%{${fg[white]}%}"
    NO_COLOR="%{${reset_color}%}"
else
    BLUE=$'%{\e[1;34m%}'
    RED=$'%{\e[1;31m%}'
    GREEN=$'%{\e[1;32m%}'
    CYAN=$'%{\e[1;36m%}'
    WHITE=$'%{\e[1;37m%}'
    MAGENTA=$'%{\e[1;35m%}'
    YELLOW=$'%{\e[1;33m%}'
    NO_COLOR=$'%{\e[0m%}'
fi

#function Z_Prompt() {
    #if [ "$UID" = "0" ]
    #then
        #PROMPT="[ %{$fg[red]%}%n%{$reset_color%}@%{$fg[cyan]%}%M%{$reset_color%} %(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)% %{$reset_color%} ] %{$fg[green]%}%d %{$fg[red]%}∮%{$reset_color%} "
    #else
        ##PROMPT="[ %{$fg[magenta]%}%n%{$reset_color%}@%{$fg[cyan]%}%M%{$reset_color%} %(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)% %{$reset_color%} ] %{$fg[green]%}$(python3 ~/usr/lib/zsh/bpath.py `pwd`) ∫%{$reset_color%} "
        #PROMPT="[ %{$fg[magenta]%}%n%{$reset_color%}@%{$fg[cyan]%}%M%{$reset_color%} %(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)% %{$reset_color%} ] %{$fg[green]%}%4~ \

 #∫%{$reset_color%} "
    #fi
    #RPROMPT="[ %W %@ ]"
#}

function Z_Prompt(){
    USER_CLR="${MAGENTA}"
    zstyle ':vcs_info:*' enable git fossil
    zstyle ':vcs_info:git:*' formats "${BLUE}%r${NO_COLOR}" "${MAGENTA}%b${NO_COLOR}"
    precmd(){vcs_info}
#    if [ "$UID" = "0" ] ; then
#	USER_CLR="${RED}"
#        PROMPT="${RED}⌠ [ %n${NO_COLOR}@${CYAN}%M ]${NO_COLOR} ${vcs_info_msg_0_} ${GREEN}%4~ \
#
#${RED}⌡ (%(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)${BLUE}) ${NO_COLOR} "
#    else
#        PROMPT="${BLUE}⌠ [ %n${NO_COLOR}@${CYAN}%M ]${NO_COLOR} ${vcs_info_msg_0_} ${GREEN}%4~ \
#
#${BLUE}⌡ (%(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)${BLUE}) ${NO_COLOR} "
#    fi
    if [ "$UID" = "0" ] ; then
	USER_CLR="${RED}"
    fi

    PROMPT="%4~ ${BLUE}(%(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)${BLUE}) $ ${NO_COLOR}"
#    PROMPT="${USER_CLR}⌠ %n${NO_COLOR}@${WHITE}%M ${NO_COLOR}${GREEN}%4~ ${BLUE}${vcs_info_msg_0_}\
#
#${USER_CLR}⌡ ${USER_CLR}|%(?.%{$fg[green]%}%?.%B%{$fg[red]%}%?%b)${USER_CLR}|${NO_COLOR} "

#    RPROMPT=" ${vcs_info_msg_1_} ${CYAN}[ %W %@ ]${NO_COLOR}"

}

#function Z_Prompt(){
    #zstyle ':vcs_info:*' enable git fossil
    #zstyle ':vcs_info:git:*' formats "${BLUE}(%s)${NO_COLOR}-${GREEN}[%b]${NO_COLOR}"

#}
