#!/bin/zsh
DIR=$(dirname $(realpath $0))

if [ -z "$1" -a -f $HOME/.zshenv ] ; then
    echo Unable to install over top existing $HOME/.zshenv
    exit 1
fi

IFS='' read -d '' -r NEW_ENV <<EOF
#!/bin/zsh

# Autogenerated ENV file to point back to zshrc git repo
# ZSH_HOME should point back to the root of the zshrc repo

ZSH_HOME=$DIR
if [ ! -d "\$ZSH_HOME" ] ; then
   echo Could not locate \$ZSH_HOME
else
   export ZDOTDIR=\$ZSH_HOME
   source \$ZDOTDIR/.zshenv
fi

EOF

if [ "$1" = "-p" ] ; then
  echo $NEW_ENV
  else
echo $NEW_ENV > $HOME/.zshenv
fi
