#!/bin/sh

# Set to the file name in banners that you want printed at start up

export BANNER=""

# Set to the file name in prompts that you want to use

export ZSH_PROMPT="zprompt.zsh"
