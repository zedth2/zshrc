
#Begin PATH Additions...


#...End PATH Additions


#IS_SERVER=0
# We will use TMUX_NAME to auto reattach to it if it exists
#TMUX_NAME="main"
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000


if [ -n "$HOME/usr/glib/dotfiles/env" ]
then
	export $(envsubst < "$HOME/usr/glib/dotfiles/env")
fi
