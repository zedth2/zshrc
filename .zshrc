#!/bin/zsh
# The following lines were added by compinstall

ZSHRC=~/.zshrc
if [ -n "$ZDOTDIR" ] ; then
    ZSHRC=$ZDOTDIR/.zshrc
fi

SHELL_DIR=$(dirname $ZSHRC)
CFG_PATH=$SHELL_DIR/local.sh
PROMPT_PATH=$SHELL_DIR/prompts
BANNER_PATH=$SHELL_DIR/banners

if [ -f "$CFG_PATH" ] ; then
    . $CFG_PATH
elif [ -f "$SHELL_DIR/local.example.sh" ] ; then
    . "$SHELL_DIR/local.example.sh"
fi

#autoload -U add-zsh-hook

#look at g_zshrc 1681

WORDCHARS="*?_-.[]=!#%^(){}<>"

export FPATH=$FPATH:$SHELL_DIR/fn

autoload -Uz check_com #This function will check commands in PATH alias, and other things.
#distro=$(grep \)\ distro= `which screenfetch` | grep -v \{ | awk -F \" '{ print $2 }' | sort -u -R | head -n 1)

#screenfetch -A $distro

ZSH_PROMPT="zprompt-one.zsh"
if [ -n "$ZSH_PROMPT" -a -f "$PROMPT_PATH/$ZSH_PROMPT" ] ; then
    source $PROMPT_PATH/$ZSH_PROMPT
else
    source $PROMPT_PATH/zprompt.zsh
fi

#source $SHELL_DIR/zprompt-one.zsh

NO_COLOR="%{${reset_color}%}"

autoload colors; colors
COLS="$(tput cols)"
LEN=79
echo $fg[green]
COWSAY=cowsay

HOSTNAME="$(hostnamectl hostname)"

if [ -n "$BANNER" -a -f "$BANNER_PATH/$BANNER" ] ; then
    sed -r '/^#/d' "$BANNER_PATH/$BANNER"
elif check_com $COWSAY; then
    line="Welcome $(whoami) to $HOSTNAME"
    end=""
    if check_com lolcat ; then
	$COWSAY -f dragon-and-cow $line | lolcat -p .1
    else
	$COWSAY $line
    fi
elif check_com neofetch ; then
    neofetch
else
    echo Welcome $(whoami) to $HOSTNAME
fi

echo $reset_color

function center_print(){
    #For some reason it doesn't like the skull
    while IFS= read -r line; do
	printf "%*s\n" $(( (${#line} + COLS) / 2)) "$line"
    done <<< "$DELOREAN"
}


#Begin Syntax highlighting things...
#function pygmentize_cat {
  #for arg in "$@"; do
    #pygmentize -g "${arg}" 2> /dev/null || /bin/cat "${arg}"
  #done
#}
#alias cat=pygmentize_cat

#export LESSOPEN="| /usr/bin/src-hilite-lesspipe.sh %s"
#export LESS=' -R '
#export GCC_COLORS="always"
#...End Syntax highlighting things.

if [ -d $HOME/usr/bin ]
then
    PATH=$PATH:$HOME/usr/bin
fi

if [ -d $HOME/usr/local/bin ]
then
    PATH=$PATH:$HOME/usr/local/bin
fi

if [ -d $HOME/usr/debin ]
then
    PATH=$PATH:$HOME/usr/debin
fi

autoload -Uz compinit && compinit
autoload -Uz vcs_info
autoload -Uz ranger-cd
autoload -Uz jmp
autoload -Uz gfetch
autoload -U +X bashcompinit && bashcompinit
alias -g rcd="ranger-cd"
zstyle ':completion:*' completer _complete
zstyle ':completion:*:descriptions'    format $'%{\e[0;31m%}completing %B%d%b%{\e[0m%}'
#_complete _ignored _approximate
zstyle ':completion:*' menu select
zstyle ':completion:*:default'         list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' max-errors 5 numeric
zstyle ':completion:*' menu select=5
zstyle ':completion:*:matches'         group 'yes'
zstyle ':completion:*'                 group-name ''

#ohhhh this is just sexyyyy
zstyle ':completion:*:manuals'    separate-sections true
zstyle ':completion:*:manuals.*'  insert-sections   true
zstyle ':completion:*:man:*'      menu yes select
zstyle :compinstall filename '$HOME/.zshrc'

if [ -e "`which kubectl 2> /dev/null`" ] ; then
    source <(kubectl completion zsh)
fi
if [ -e "`which minikube 2> /dev/null`" ] ; then
    source <(minikube completion zsh)
fi


if [ -e "/usr/share/bash-completion/completions/virt-install" ] ; then
	source "/usr/share/bash-completion/completions/virt-install"
fi

zstyle ':completion:*' rehash true


# End of lines added by compinstall
# Lines configured by zsh-newuser-install

setopt notify
setopt HIST_IGNORE_ALL_DUPS
setopt EXTENDED_HISTORY
setopt SHARE_HISTORY
setopt EXTENDED_GLOB
setopt PRINT_EXIT_VALUE
setopt AUTO_CD
setopt CDABLE_VARS
setopt PROMPT_SUBST

setopt nonomatch
setopt completeinword
setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS

# support colors in less
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#Keystrokes Begin...
bindkey -e
#[[ -n ${key[End]} ]] && bindkey "${key[End]}"  end-of-line
bindkey "${terminfo[kend]}"  end-of-line
#bindkey "^[OF" end-of-line
#bindkey "^[OH" beginning-of-line
#[[ -n ${key[Home]} ]] && bindkey "${key[Home]}"  beginning-of-line
bindkey "${terminfo[khome]}"  beginning-of-line
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5A" beginning-of-history
bindkey "^[[1;5B" end-of-history
#bindkey "^[[1;\b" backward-word
bindkey "^[[3~" delete-char
[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" delete-char
[[ -n ${key[Backspace]} ]] && bindkey "^[[1;${key[Backspace]}" backward-delete-word
#...End Keystrokes
# End of lines configured by zsh-newuser-install


#Begin Python Modifications...
#export PYTHONDONTWRITEBYTECODE=no

#...End Python Modifications



#Begin Aliases...


#if [ -z "$TTY" ] && check_com emacs ; then
#    export EDITOR=emacs
if check_com nvim ; then
    export EDITOR=nvim
else
    export EDITOR=vi
fi

if check_com alacritty ; then
    export TERMINAL="alacritty"
elif check_com kitty ; then
    export TERMINAL="kitty"
fi


#Aliases for Compatibility between BSD and Linux
if [ `uname` = 'Linux' ]
then
    alias -g ls="ls -F --color=auto"
    alias -g grep="grep --color"
elif [ `uname` = 'FreeBSD' ]
then
    #alias -g grep="grep -n"
fi

#Begin Short Hands Aliases...
alias -g pf="ps aux | grep"
#alias -g eh="echo"
#alias fu="sudo"
#alias fuck="sudo !!"
#alias -g lns="ln -s"
#alias -g lnsp="ln -s ${PWD}"
alias p3="python3"
#alias p2="python2"
alias fixn="stty sane"
alias em="emacs -nw"
#...End Short Hands Aliases


alias d="dirs -v"
alias 0="cd -0"
alias 1="cd -1"
alias 2="cd -2"
alias 3="cd -3"
alias 4="cd -4"

if check_com ag ; then
    alias -g gp="ag"
    alias -g gpy="ag --python"
    alias -g gpc="ag --cc"
    alias -g gpcp="ag --cpp"
    alias -g gpjs="ag --js"
    alias -g gpj="ag --java"
else
    alias -g gp="grep -n -rI -P"
    alias -g gpy="gp --include \*.py"
    alias -g gpc="gp --include \*.c --include \*.cc --include \*.h"
    alias -g gpcp="gp --include \*.cpp --include \*.h"
    alias -g gps="gp --include \*.cs"
    alias -g gpj="gp --include \*.java"
fi

if check_com docker ; then
    alias dck="docker"
    alias dps="docker ps"
    alias dimg="docker images"
fi
if check_com docker-compose ; then
    alias dc="docker-compose"
    alias dcps="docker-compose ps"
fi


#Begin File System Aliases...
alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'

#export sg="/run/media/$USER"

if check_com eza ; then
    alias ls="eza"
    alias -g la="eza -a"
    alias -g lla="eza -la"
    alias -g ll="eza -l"
    alias -g lg="eza --git"
    alias -g llg="eza -l --git"
else
    alias -g la="ls -a"
    alias -g lla="ls -lah"
    alias -g ll="ls -lh"
fi

alias wl="wc -l"
alias etc="cd /etc"
[ -d $HOME/usr/bin ] && alias hbin=$HOME/usr/bin
[ -d $HOME/usr/lib ] && alias hlib=$HOME/usr/lib
alias dz="du -d 1 -h"
#...End File System Aliases

#Begin Pipe Aliases...
alias -g NUL="> /dev/null 2>&1"
alias -g G="| grep"
alias -g L="| less"
#...End Pipe Aliases

alias g++="g++ -fdiagnostics-color=always"

#Begin Git Aliases...
if check_com git
then
    alias g="git"
    alias -g gcl="git clone"
    alias -g gch="git checkout"
    alias gst="git status"
    alias gadd="git add"
    alias gdif="git diff"
    alias gmt="git commit"
    alias fetch="git fetch --all"
fi
#...End Git Aliases

#Begin Fossil Aliases...
if check_com fossil
then
    autoload -Uz _fossil
    alias fsl="fossil"
    alias fmt="fossil commit"
    alias fpl="fossil pull"
    alias fyc="fossil sync"
    alias fup="fossil update"
    alias fst="fossil status"
    alias fmv="fossil mv"
    alias fco="fossil co"
    alias fadd="fossil add"
    alias fls="fossil \ls"
fi
#...End Fossil Aliases

if check_com jd
then
    alias jd=". jd"
fi

#Package Mangement
DISTRO="`cat /etc/os-release | grep ID= | cut -d = -f 2 | head -1`"
if check_com dnf
then
    alias -g pS="sudo dnf install"
    alias -g ys="dnf search"
    alias -g yrm="sudo dnf remove"
    alias -g yiy="sudo dnf -y install"
    alias -g ywp="dnf provides"
    alias -g yl="dnf list"
fi

if check_com pacman
then
    alias -g pSs="pacman -Ss" #Search for package
    alias -g pS="sudo pacman -S" #Install package
    alias -g pQ="sudo pacman -Q"
    alias -g pQl="sudo pacman -Ql"
fi

if check_com yay ; then
    alias -g ySs="yay -Ss"
    alias -g yS="yay -S"
fi

if check_com apt
then
    alias -g api="sudo apt install"
    alias -g aps="apt search"
    alias -g apu="sudo apt update"
fi

if check_com cargo
then
    alias cgo="cargo"
    alias cnew="cargo new"
    alias crun="cargo run"
    alias cbuild="cargo build"
    alias cclean="cargo clean"
fi

#Extra commands

export 	GUILE_LOAD_PATH="...:$HOME/Documents/code/libs"


# Do not send telemtry back to microsoft
DOTNET_CLI_TELEMETRY_OPTOUT=1


#...End Aliases


ANDROID_EMULATOR_USE_SYSTEM_LIBS=1

PATH="$HOME/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

#source $SHELL_DIR/schl.zsh
zshHigh="/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
ubuntuZshHigh="/usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
if [ -e $zshHigh ]
then
	source $zshHigh
elif [ -e $ubuntuZshHigh ]
then
	 source $ubuntuZshHigh
fi

zshAuto="/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"
ubuntuZshAuto="/usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
if [ -e $zshAuto ]
then
    source $zshAuto
elif [ -e $ubuntuZshAuto ]
then
    source $ubuntuZshAuto
fi
#export JAVA_HOME=/usr/lib/jvm/default
#export LANG=en_US.UTF-8
#export LC_MESSAGES="C"
#export LD_LIBRARY_PATH=/usr/local/Aria/lib:$LD_LIBRARY_PATH

function start_tmux() {
    if ! check_com tmux || [ "$TMUX" ] ; then
	return
    fi
    if (( ${+TMUX_NAME} )); then
	tmux new-session -A -s $TMUX_NAME
    else
	tmux new-session
    fi
}

if (( ${+IS_SERVER} )) ; then
    start_tmux
else
    if [[ -z "$DISPLAY" ]] && [[ "$TTY" = "/dev/tty1" ]]
    then
	exec startx
    else
	start_tmux
    fi
fi



# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
#__conda_setup="$('/usr/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
#if [ $? -eq 0 ]; then
#    eval "$__conda_setup"
#else
#    if [ -f "/usr/etc/profile.d/conda.sh" ]; then
#        . "/usr/etc/profile.d/conda.sh"
#    else
#        export PATH="/usr/bin:$PATH"
#    fi
#fi
#unset __conda_setup
# <<< conda initialize <<<


